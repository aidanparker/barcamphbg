$(function(){
    var introVid = $('#intro-vid');
    var logoSVG = $('#logo-svg');
    var fadeTime = 200;

    var fullPageContainer = $('#fullpage');

    fullPageContainer.fullpage({
        anchors: ['home','join-us','what-is-it','whats-it-like','sponsors','contact'],
        menu: '#menu',
        loopTop: true,
        loopBottom: true
    });

    var menu = $('#menu');
    var menuSmall = $('#menu-small');
    var menuBtn = $('#menu-btn');
    var menuAnchor = $('#menu a');
    var menuSmallAnchor = $('#menu-small a');

    if(introVid.css('display') != 'none'){
      logoSVG.hide();
    }else {
      logoSVG.show();
      menuSmall.show();
    }

    //Intro Video Fadeout
    introVid.on('ended',function(){
        logoSVG.show();
        introVid.fadeOut(fadeTime);
        menu.fadeIn(fadeTime);
        menuSmall.fadeIn(fadeTime * 2);
    });

    introVid.on('pause',function(){
        logoSVG.show();
        introVid.fadeOut(fadeTime);
        menu.fadeIn(fadeTime);
        menuSmall.fadeIn(fadeTime * 2);
    });

    //Responsive Menu
    menuBtn.click(function(){
        fullPageContainer.toggleClass('open');
        menuSmallAnchor.toggleClass('close');
    });

    menuAnchor.click(function(){
        fullPageContainer.removeClass('open');
        menuSmallAnchor.removeClass('close');
    });
});
